﻿using Business.Interfaces;
using FizzBuzzDemo.Controllers;
using FizzBuzzDemo.Models;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FizzBuzzDemo.Tests.FizzBuzzControllerLogic
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private FizzBuzzController fizzBuzzController;

        private Mock<IFizzBuzzService> mockFizzBuzzService;

        private FizzBuzzViewModel fizzBuzzViewModel;

        private List<string> fizzBuzzList;

        [SetUp]
        public void SetUp_Test()
        {
            mockFizzBuzzService = new Mock<IFizzBuzzService>();
            fizzBuzzController = new FizzBuzzController(mockFizzBuzzService.Object);
            fizzBuzzList = new List<string>() { "1", "2", "Fizz", "4", "Buzz" };
        }

        [Test]
        public void FizzBuzzController_Get_Index_Action_Should_Return_Index_View_Page()
        {
            var actual = fizzBuzzController.Index() as ViewResult;

            Assert.AreEqual(actual.ViewName, "Index");
        }

        [TestCase(3)]
        [TestCase(5)]
        public void FizzBuzzController_GetFizzBuzzList_Action_Should_Return_View_With_Model_When_The_Input_Number_IsValid(int number)
        {
            int page = 1;
            fizzBuzzViewModel = new FizzBuzzViewModel() { Number = number };

            mockFizzBuzzService.Setup(x => x.GetFizzBuzzList(number)).Returns(fizzBuzzList);
            var result = fizzBuzzController.GetFizzBuzzList(fizzBuzzViewModel, page) as ViewResult;

            Assert.AreEqual(result.Model, fizzBuzzViewModel);
            Assert.AreEqual(result.ViewName, "Index");

            mockFizzBuzzService.Verify(x => x.GetFizzBuzzList(number), Times.Exactly(1));
        }

        [TestCase(0)]
        [TestCase(1001)]
        public void FizzBuzzController_GetFizzBuzzList_Action_Should_Return_ModelStateValidationMessage_When_The_Input_Number_Is_InValid(int number)
        {
            int page = 1;
            fizzBuzzViewModel = new FizzBuzzViewModel() { Number = number };

            fizzBuzzController.ModelState.AddModelError("Number", "Please enter a number between 0 and 1001");

            mockFizzBuzzService.Setup(x => x.GetFizzBuzzList(number)).Returns(fizzBuzzList);
            var result = fizzBuzzController.GetFizzBuzzList(fizzBuzzViewModel, page) as ViewResult;

            Assert.AreEqual(result.Model, fizzBuzzViewModel);
            Assert.AreEqual(result.ViewName, "Index");

            mockFizzBuzzService.Verify(x => x.GetFizzBuzzList(number), Times.Never);
        }

        [TearDown]
        public void TearDown_Test()
        {

        }
    }
}
