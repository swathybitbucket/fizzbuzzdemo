﻿using Business.FizzBuzzService;
using Business.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzzDemo.Tests.FizzBuzzServiceLogic
{
    [TestFixture]
    public class FizzBuzzServiceTest
    {
        private FizzBuzzService fizzBuzzService;

        private Mock<IDivisibilityRule> divisibilityByThree;

        private Mock<IDivisibilityRule> divisibilityByFive;

        private IList<IDivisibilityRule> divisibilityRules;

        [SetUp]
        public void SetUp_Test()
        {
            divisibilityByFive = new Mock<IDivisibilityRule>();
            divisibilityByThree = new Mock<IDivisibilityRule>();
            divisibilityRules = new List<IDivisibilityRule>() { divisibilityByFive.Object, divisibilityByThree.Object };
            fizzBuzzService = new FizzBuzzService(divisibilityRules);
        }

        [TestCase(7)]
        public void GetFizzBuzzList_Should_Return_ListOfString_When_The_Input_Number_IsValid(int number)
        {
            var actual = fizzBuzzService.GetFizzBuzzList(number);

            Assert.Greater(number, 0, "Please eneter a number greater than 0");
            Assert.Less(number, 1001, "Please enter a number less than 1001");
            Assert.IsInstanceOf<List<string>>(actual);
        }

        [Test]
        public void GetFizzBuzzList_Should_Return_ListOfString_Contains_ListOfThree_Items_When_The_NumberIsThree()
        {
            divisibilityByThree.Setup(x => x.IsDivisible(3)).Returns(true);
            divisibilityByThree.Setup(x => x.GetMessage()).Returns("Fizz");

            var result = fizzBuzzService.GetFizzBuzzList(3);

            Assert.AreEqual(result[0], "1");
            Assert.AreEqual(result[1], "2");
            Assert.AreEqual(result[2], "Fizz");

            divisibilityByThree.Verify(x => x.IsDivisible(It.IsAny<int>()), Times.Exactly(3));
        }

        [Test]
        public void GetFizzBuzzList_Should_Return_ListOfString_Contains_ListOfFive_Items_When_The_NumberIsFive()
        {
            divisibilityByThree.Setup(x => x.IsDivisible(3)).Returns(true);
            divisibilityByThree.Setup(x => x.GetMessage()).Returns("Fizz");

            divisibilityByFive.Setup(x => x.IsDivisible(5)).Returns(true);
            divisibilityByFive.Setup(x => x.GetMessage()).Returns("Buzz");

            var result = fizzBuzzService.GetFizzBuzzList(5);

            Assert.AreEqual(result[0], "1");
            Assert.AreEqual(result[1], "2");
            Assert.AreEqual(result[2], "Fizz");
            Assert.AreEqual(result[3], "4");
            Assert.AreEqual(result[4], "Buzz");

            divisibilityByThree.Verify(x => x.IsDivisible(It.IsAny<int>()), Times.Exactly(5));
        }

        [TearDown]
        public void TearDown_Test()
        {
            divisibilityByFive = null;
            divisibilityByThree = null;
            divisibilityRules = null;
            fizzBuzzService = null;
        }
    }
}
