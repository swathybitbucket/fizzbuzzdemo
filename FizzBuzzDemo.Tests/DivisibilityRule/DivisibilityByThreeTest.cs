﻿using System;
using Business.DivisibilityRule;
using Business.Interfaces;
using Moq;
using NUnit.Framework;

namespace FizzBuzzDemo.Tests.DivisibilityRule
{
    [TestFixture]
    public class DivisibilityByThreeTest
    {
        private Mock<IDayCheck> mockDayCheck;

        private DivisibilityByThree divisibilityByThree;

        [SetUp]
        public void SetUp_Test()
        {
            mockDayCheck = new Mock<IDayCheck>();
            divisibilityByThree = new DivisibilityByThree(mockDayCheck.Object);
        }

        [TestCase(15,true)]
        public void Check_IsNumber_Divisible_Should_Return_True_When_The_Input_Number_Is_Divisible_By_Three(int number, bool expected)
        {
            var actual = divisibilityByThree.IsDivisible(number);

            Assert.IsInstanceOf<bool>(actual, "Please enter a valid number");
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Fizz")]
        public void GetMessage_Should_Return_Fizz_When_DayOfWeek_IsNotWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsDayMatch(It.IsAny<DayOfWeek>())).Returns(false);

            var actual = divisibilityByThree.GetMessage();

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Wizz")]
        public void GetMessage_Should_Return_Wizz_When_DayOfWeek_IsWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsDayMatch(It.IsAny<DayOfWeek>())).Returns(true);

            var actual = divisibilityByThree.GetMessage();

            Assert.AreEqual(expected, actual);
        }

        [TearDown]
        public void TearDown_Test()
        {
            mockDayCheck = null;
            divisibilityByThree = null;
        }
    }
}
