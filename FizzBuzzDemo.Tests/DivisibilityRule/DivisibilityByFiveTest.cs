﻿using Business.DivisibilityRule;
using Business.Interfaces;
using Moq;
using NUnit.Framework;
using System;

namespace FizzBuzzDemo.Tests.DivisibilityRule
{
    [TestFixture]
    public class DivisibilityByFiveTest
    {
        private Mock<IDayCheck> mockDayCheck;

        private DivisibilityByFive divisibilityByFive;

        [SetUp]
        public void SetUp_Test()
        {
            mockDayCheck = new Mock<IDayCheck>();
            divisibilityByFive = new DivisibilityByFive(mockDayCheck.Object);
        }

        [TestCase(5, true)]
        public void Check_IsNumber_Divisible_Should_Return_True_When_The_Input_Number_Is_Divisible_By_Three(int number, bool expected)
        {
            var actual = divisibilityByFive.IsDivisible(number);

            Assert.IsInstanceOf<bool>(actual, "Please enter a valid number");
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Buzz")]
        public void GetMessage_Should_Return_Buzz_When_DayOfWeek_IsNotWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsDayMatch(It.IsAny<DayOfWeek>())).Returns(false);

            var actual = divisibilityByFive.GetMessage();

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Wuzz")]
        public void GetMessage_Should_Return_Wuzz_When_DayOfWeek_IsWednesday(string expected)
        {
            mockDayCheck.Setup(x => x.IsDayMatch(It.IsAny<DayOfWeek>())).Returns(true);

            var actual = divisibilityByFive.GetMessage();

            Assert.AreEqual(expected, actual);
        }

        [TearDown]
        public void TearDown_Test()
        {
            mockDayCheck = null;
            divisibilityByFive = null;
        }
    }
}
