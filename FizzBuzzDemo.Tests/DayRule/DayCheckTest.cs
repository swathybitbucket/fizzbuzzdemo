﻿using Business.Interfaces;
using Moq;
using NUnit.Framework;
using System;

namespace FizzBuzzDemo.Tests.DayRule
{
    [TestFixture]
    public class DayCheckTest
    {
        private Mock<IDayCheck> mockDayCheck;

        [SetUp]
        public void SetUp_Test()
        {
            mockDayCheck = new Mock<IDayCheck>();
        }

        [TestCase(DayOfWeek.Wednesday, true)]
        [TestCase(DayOfWeek.Monday, false)]
        public void DayCheck_IsMatch_Should_Return_True_When_Input_Day_Is_Wednesday_Otherwise_False(DayOfWeek dayOfWeek, bool isMatch)
        {
            mockDayCheck.Setup(x => x.IsDayMatch(dayOfWeek)).Returns(isMatch);

            var actual = mockDayCheck.Object.IsDayMatch(dayOfWeek);

            Assert.AreEqual(isMatch, actual);
        }

        [TearDown]
        public void TearDown_Test()
        {
            mockDayCheck = null;
        }
    }
}
