﻿using Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DivisibilityRule
{
    public class DivisibilityByThree : IDivisibilityRule
    {
        private readonly IDayCheck dayCheck;

        public DivisibilityByThree(IDayCheck dayCheck)
        {
            this.dayCheck = dayCheck;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0;
        }

        public string GetMessage()
        {
            return dayCheck.IsDayMatch(DateTime.UtcNow.DayOfWeek) ? "Wizz": "Fizz";
        }
    }
}
