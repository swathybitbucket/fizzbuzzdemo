﻿using Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DivisibilityRule
{
    public class DivisibilityByFive: IDivisibilityRule
    {
        private readonly IDayCheck dayCheck;

        public DivisibilityByFive(IDayCheck dayCheck)
        {
            this.dayCheck = dayCheck;
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0;
        }

        public string GetMessage()
        {
            return dayCheck.IsDayMatch(DateTime.UtcNow.DayOfWeek) ? "Wuzz" : "Buzz";
        }
    }
}
