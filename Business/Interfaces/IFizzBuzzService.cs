﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Interfaces
{
    public interface IFizzBuzzService
    {
        List<string> GetFizzBuzzList(int number);
    }
}
