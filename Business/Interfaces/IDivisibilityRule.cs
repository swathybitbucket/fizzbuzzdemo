﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Interfaces
{
    public interface IDivisibilityRule
    {
        bool IsDivisible(int number);

        string GetMessage();
    }
}
