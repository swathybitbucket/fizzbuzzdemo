﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Interfaces
{
    public interface IDayCheck
    {
        bool IsDayMatch(DayOfWeek dayOfWeek);
    }
}
