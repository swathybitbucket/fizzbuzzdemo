﻿using Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DayRule
{
    public class DayCheck: IDayCheck
    {
        public bool IsDayMatch(DayOfWeek dayOfWeek)
        {
            return DayOfWeek.Wednesday == dayOfWeek ? true : false;
        }
    }
}
