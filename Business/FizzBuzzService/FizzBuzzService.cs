﻿using Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.FizzBuzzService
{
    public class FizzBuzzService: IFizzBuzzService
    {
        private readonly IEnumerable<IDivisibilityRule> divisibilityRule;

        public FizzBuzzService(IEnumerable<IDivisibilityRule> divisibilityRule)
        {
            this.divisibilityRule = divisibilityRule;
        }

        public List<string> GetFizzBuzzList(int number)
        {
            List<string> fizzBuzzList = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                var isDivisible = divisibilityRule.Where(m => m.IsDivisible(i)).ToList();
                if (isDivisible.Any())
                {
                    fizzBuzzList.Add(string.Join(" ", isDivisible.Select(m => m.GetMessage())));
                }
                else
                {
                    fizzBuzzList.Add(i.ToString());
                }
            }

            return fizzBuzzList;
        }
    }
}
