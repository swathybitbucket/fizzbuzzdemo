﻿using Business.Interfaces;
using FizzBuzzDemo.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzzDemo.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService fizzBuzzService;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        // GET: FizzBuzz
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult GetFizzBuzzList(FizzBuzzViewModel model, int? page)
        {
            if (ModelState.IsValid)
            {
                int pageNumber = page != null ? Convert.ToInt32(page) : 1;
                int pageSize = 10;
                var fizzBuzzList = this.fizzBuzzService.GetFizzBuzzList(model.Number);
                if (fizzBuzzList != null)
                {
                    model.FizzBuzzList = fizzBuzzList.ToPagedList(pageNumber, pageSize);
                }
            }

            return View("Index", model);
        }
    }
}