﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzzDemo.Models
{
    public class FizzBuzzViewModel
    {
        [DisplayName("Please enter a number")]
        [Required(ErrorMessage ="Please enter a number")]
        [Range(1, 1000, ErrorMessage ="Please enter a number between 0 and 1000")]
        public int Number { get; set; }

        public IPagedList<string> FizzBuzzList { get; set; }
    }
}